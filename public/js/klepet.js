// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

function spremeniBarvo(barva) {
  document.getElementById("sporocila").style.color = barva;
  document.getElementById("kanal").style.color = barva;
}

function ytVideo(sporocilo) {
  var message = sporocilo;
  if (sporocilo.indexOf("https://www.youtube.com/watch?v=") > -1) {
    message = message + '<div class="videos">';
    var words = sporocilo.split(" ");
    
    var counter = 0;
    var ytVideos = [];
    for(var i = 0; i < words.length; i++) {
      if(words[i].indexOf("https://www.youtube.com/watch?v=") > -1) {
        var ytId = words[i].split("https://www.youtube.com/watch?v=");
        ytVideos[counter] = ytId[1];
        counter++;
      }
    }
    
    for(var i = 0; i < ytVideos.length; i++) {
      message = message + '<iframe src="https://www.youtube.com/embed/' + ytVideos[i] + '" width="200px" height="150px" style="margin: 10px 0px 0px 10px" allowfullscreen></iframe>';
    }
    message = message + "</div>";
  }
  return message;
}

/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'barva':
      besede.shift();
      var barva = besede.join(' ');
      spremeniBarvo(barva);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      var msg = parametri[3];
      msg = ytVideo(msg);
      if (parametri) {
        this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + msg;
        
        var vzdevki = parametri[1].split(",");
        for(var i = 0; i < vzdevki.length; i++) {
          var res = parametri[3].split(" ");
          for(var j = 0; j < res.length; j++) {
            if(res[j].charAt(0) == '@') {
              var tagged = res[j].substr(1);
              if(tagged == vzdevki[i]) {
                this.socket.emit('sporocilo', {vzdevek: vzdevki[i], besedilo: "&#9758; Omemba v klepetu"});
              }
            }
          }
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};